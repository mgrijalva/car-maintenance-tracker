﻿namespace Car_Maintenance_Tracker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textbox_engineoil = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textbox_tranoil = new System.Windows.Forms.TextBox();
            this.textbox_diffoil = new System.Windows.Forms.TextBox();
            this.textbox_tirerot = new System.Windows.Forms.TextBox();
            this.textbox_drivebelts = new System.Windows.Forms.TextBox();
            this.textbox_timingbelt = new System.Windows.Forms.TextBox();
            this.textbox_coolant = new System.Windows.Forms.TextBox();
            this.textbox_fuelfilter = new System.Windows.Forms.TextBox();
            this.textbox_aircleaner = new System.Windows.Forms.TextBox();
            this.textbox_sparkplugs = new System.Windows.Forms.TextBox();
            this.textbox_brakefluid = new System.Windows.Forms.TextBox();
            this.label_title = new System.Windows.Forms.Label();
            this.textbox_changebrakefluid = new System.Windows.Forms.TextBox();
            this.textbox_changesparkplugs = new System.Windows.Forms.TextBox();
            this.textbox_changeaircleaner = new System.Windows.Forms.TextBox();
            this.textbox_changefuel = new System.Windows.Forms.TextBox();
            this.textbox_changecoolant = new System.Windows.Forms.TextBox();
            this.textbox_changetiming = new System.Windows.Forms.TextBox();
            this.textbox_changedrivebelts = new System.Windows.Forms.TextBox();
            this.textbox_changetirerot = new System.Windows.Forms.TextBox();
            this.textbox_changediffoil = new System.Windows.Forms.TextBox();
            this.textbox_changetranoil = new System.Windows.Forms.TextBox();
            this.textbox_changeengineoil = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label_changeatoil = new System.Windows.Forms.Label();
            this.label_changeattranoil = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label_changeatDiff = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textbox_notes = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCarNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textbox_engineoil
            // 
            this.textbox_engineoil.Location = new System.Drawing.Point(183, 107);
            this.textbox_engineoil.MaxLength = 6;
            this.textbox_engineoil.Name = "textbox_engineoil";
            this.textbox_engineoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_engineoil.TabIndex = 1;
            this.textbox_engineoil.Tag = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Engine Oil";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Transmission Oil";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Differential Oil";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Tire Rotation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(80, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Drive Belts";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(80, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Timing Belt";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(80, 278);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Coolant";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(80, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Fuel Filter";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(80, 334);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Air Cleaner";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(80, 362);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Spark Plugs";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(80, 390);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Brake Fluid";
            // 
            // textbox_tranoil
            // 
            this.textbox_tranoil.Location = new System.Drawing.Point(183, 138);
            this.textbox_tranoil.MaxLength = 6;
            this.textbox_tranoil.Name = "textbox_tranoil";
            this.textbox_tranoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_tranoil.TabIndex = 2;
            this.textbox_tranoil.Tag = "2";
            // 
            // textbox_diffoil
            // 
            this.textbox_diffoil.Location = new System.Drawing.Point(183, 166);
            this.textbox_diffoil.MaxLength = 6;
            this.textbox_diffoil.Name = "textbox_diffoil";
            this.textbox_diffoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_diffoil.TabIndex = 3;
            this.textbox_diffoil.Tag = "3";
            // 
            // textbox_tirerot
            // 
            this.textbox_tirerot.BackColor = System.Drawing.SystemColors.Window;
            this.textbox_tirerot.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textbox_tirerot.Location = new System.Drawing.Point(183, 194);
            this.textbox_tirerot.MaxLength = 6;
            this.textbox_tirerot.Name = "textbox_tirerot";
            this.textbox_tirerot.Size = new System.Drawing.Size(100, 20);
            this.textbox_tirerot.TabIndex = 4;
            this.textbox_tirerot.Tag = "4";
            // 
            // textbox_drivebelts
            // 
            this.textbox_drivebelts.Location = new System.Drawing.Point(183, 222);
            this.textbox_drivebelts.MaxLength = 6;
            this.textbox_drivebelts.Name = "textbox_drivebelts";
            this.textbox_drivebelts.Size = new System.Drawing.Size(100, 20);
            this.textbox_drivebelts.TabIndex = 5;
            this.textbox_drivebelts.Tag = "5";
            // 
            // textbox_timingbelt
            // 
            this.textbox_timingbelt.Location = new System.Drawing.Point(183, 250);
            this.textbox_timingbelt.MaxLength = 6;
            this.textbox_timingbelt.Name = "textbox_timingbelt";
            this.textbox_timingbelt.Size = new System.Drawing.Size(100, 20);
            this.textbox_timingbelt.TabIndex = 6;
            this.textbox_timingbelt.Tag = "6";
            // 
            // textbox_coolant
            // 
            this.textbox_coolant.Location = new System.Drawing.Point(183, 278);
            this.textbox_coolant.MaxLength = 6;
            this.textbox_coolant.Name = "textbox_coolant";
            this.textbox_coolant.Size = new System.Drawing.Size(100, 20);
            this.textbox_coolant.TabIndex = 7;
            this.textbox_coolant.Tag = "7";
            // 
            // textbox_fuelfilter
            // 
            this.textbox_fuelfilter.Location = new System.Drawing.Point(183, 306);
            this.textbox_fuelfilter.MaxLength = 6;
            this.textbox_fuelfilter.Name = "textbox_fuelfilter";
            this.textbox_fuelfilter.Size = new System.Drawing.Size(100, 20);
            this.textbox_fuelfilter.TabIndex = 8;
            this.textbox_fuelfilter.Tag = "8";
            // 
            // textbox_aircleaner
            // 
            this.textbox_aircleaner.Location = new System.Drawing.Point(183, 334);
            this.textbox_aircleaner.MaxLength = 6;
            this.textbox_aircleaner.Name = "textbox_aircleaner";
            this.textbox_aircleaner.Size = new System.Drawing.Size(100, 20);
            this.textbox_aircleaner.TabIndex = 9;
            this.textbox_aircleaner.Tag = "9";
            // 
            // textbox_sparkplugs
            // 
            this.textbox_sparkplugs.Location = new System.Drawing.Point(183, 361);
            this.textbox_sparkplugs.MaxLength = 6;
            this.textbox_sparkplugs.Name = "textbox_sparkplugs";
            this.textbox_sparkplugs.Size = new System.Drawing.Size(100, 20);
            this.textbox_sparkplugs.TabIndex = 10;
            this.textbox_sparkplugs.Tag = "10";
            // 
            // textbox_brakefluid
            // 
            this.textbox_brakefluid.Location = new System.Drawing.Point(183, 390);
            this.textbox_brakefluid.MaxLength = 6;
            this.textbox_brakefluid.Name = "textbox_brakefluid";
            this.textbox_brakefluid.Size = new System.Drawing.Size(100, 20);
            this.textbox_brakefluid.TabIndex = 11;
            this.textbox_brakefluid.Tag = "11";
            // 
            // label_title
            // 
            this.label_title.AutoSize = true;
            this.label_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_title.Location = new System.Drawing.Point(156, 52);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(202, 26);
            this.label_title.TabIndex = 27;
            this.label_title.Text = "Load a file to start...";
            // 
            // textbox_changebrakefluid
            // 
            this.textbox_changebrakefluid.Location = new System.Drawing.Point(309, 390);
            this.textbox_changebrakefluid.MaxLength = 6;
            this.textbox_changebrakefluid.Name = "textbox_changebrakefluid";
            this.textbox_changebrakefluid.Size = new System.Drawing.Size(100, 20);
            this.textbox_changebrakefluid.TabIndex = 22;
            this.textbox_changebrakefluid.Tag = "22";
            // 
            // textbox_changesparkplugs
            // 
            this.textbox_changesparkplugs.Location = new System.Drawing.Point(309, 361);
            this.textbox_changesparkplugs.MaxLength = 6;
            this.textbox_changesparkplugs.Name = "textbox_changesparkplugs";
            this.textbox_changesparkplugs.Size = new System.Drawing.Size(100, 20);
            this.textbox_changesparkplugs.TabIndex = 21;
            this.textbox_changesparkplugs.Tag = "21";
            // 
            // textbox_changeaircleaner
            // 
            this.textbox_changeaircleaner.Location = new System.Drawing.Point(309, 334);
            this.textbox_changeaircleaner.MaxLength = 6;
            this.textbox_changeaircleaner.Name = "textbox_changeaircleaner";
            this.textbox_changeaircleaner.Size = new System.Drawing.Size(100, 20);
            this.textbox_changeaircleaner.TabIndex = 20;
            this.textbox_changeaircleaner.Tag = "20";
            // 
            // textbox_changefuel
            // 
            this.textbox_changefuel.Location = new System.Drawing.Point(309, 306);
            this.textbox_changefuel.MaxLength = 6;
            this.textbox_changefuel.Name = "textbox_changefuel";
            this.textbox_changefuel.Size = new System.Drawing.Size(100, 20);
            this.textbox_changefuel.TabIndex = 19;
            this.textbox_changefuel.Tag = "19";
            // 
            // textbox_changecoolant
            // 
            this.textbox_changecoolant.Location = new System.Drawing.Point(309, 278);
            this.textbox_changecoolant.MaxLength = 6;
            this.textbox_changecoolant.Name = "textbox_changecoolant";
            this.textbox_changecoolant.Size = new System.Drawing.Size(100, 20);
            this.textbox_changecoolant.TabIndex = 18;
            this.textbox_changecoolant.Tag = "18";
            // 
            // textbox_changetiming
            // 
            this.textbox_changetiming.Location = new System.Drawing.Point(309, 250);
            this.textbox_changetiming.MaxLength = 6;
            this.textbox_changetiming.Name = "textbox_changetiming";
            this.textbox_changetiming.Size = new System.Drawing.Size(100, 20);
            this.textbox_changetiming.TabIndex = 17;
            this.textbox_changetiming.Tag = "17";
            // 
            // textbox_changedrivebelts
            // 
            this.textbox_changedrivebelts.Location = new System.Drawing.Point(309, 222);
            this.textbox_changedrivebelts.MaxLength = 6;
            this.textbox_changedrivebelts.Name = "textbox_changedrivebelts";
            this.textbox_changedrivebelts.Size = new System.Drawing.Size(100, 20);
            this.textbox_changedrivebelts.TabIndex = 16;
            this.textbox_changedrivebelts.Tag = "16";
            // 
            // textbox_changetirerot
            // 
            this.textbox_changetirerot.Location = new System.Drawing.Point(309, 194);
            this.textbox_changetirerot.MaxLength = 6;
            this.textbox_changetirerot.Name = "textbox_changetirerot";
            this.textbox_changetirerot.Size = new System.Drawing.Size(100, 20);
            this.textbox_changetirerot.TabIndex = 15;
            this.textbox_changetirerot.Tag = "15";
            // 
            // textbox_changediffoil
            // 
            this.textbox_changediffoil.Location = new System.Drawing.Point(309, 166);
            this.textbox_changediffoil.MaxLength = 6;
            this.textbox_changediffoil.Name = "textbox_changediffoil";
            this.textbox_changediffoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_changediffoil.TabIndex = 14;
            this.textbox_changediffoil.Tag = "14";
            // 
            // textbox_changetranoil
            // 
            this.textbox_changetranoil.Location = new System.Drawing.Point(309, 138);
            this.textbox_changetranoil.MaxLength = 6;
            this.textbox_changetranoil.Name = "textbox_changetranoil";
            this.textbox_changetranoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_changetranoil.TabIndex = 13;
            this.textbox_changetranoil.Tag = "13";
            // 
            // textbox_changeengineoil
            // 
            this.textbox_changeengineoil.Location = new System.Drawing.Point(309, 107);
            this.textbox_changeengineoil.MaxLength = 6;
            this.textbox_changeengineoil.Name = "textbox_changeengineoil";
            this.textbox_changeengineoil.Size = new System.Drawing.Size(100, 20);
            this.textbox_changeengineoil.TabIndex = 12;
            this.textbox_changeengineoil.Tag = "12";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(183, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Last Time Changed";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(317, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "Change Every";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(444, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Change At";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(285, 247);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 26);
            this.label15.TabIndex = 0;
            this.label15.Text = "+";
            // 
            // label_changeatoil
            // 
            this.label_changeatoil.AutoSize = true;
            this.label_changeatoil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_changeatoil.Location = new System.Drawing.Point(456, 110);
            this.label_changeatoil.Name = "label_changeatoil";
            this.label_changeatoil.Size = new System.Drawing.Size(14, 20);
            this.label_changeatoil.TabIndex = 40;
            this.label_changeatoil.Tag = "L1";
            this.label_changeatoil.Text = "-";
            this.label_changeatoil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_changeattranoil
            // 
            this.label_changeattranoil.AutoSize = true;
            this.label_changeattranoil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_changeattranoil.Location = new System.Drawing.Point(456, 138);
            this.label_changeattranoil.Name = "label_changeattranoil";
            this.label_changeattranoil.Size = new System.Drawing.Size(14, 20);
            this.label_changeattranoil.TabIndex = 41;
            this.label_changeattranoil.Tag = "L2";
            this.label_changeattranoil.Text = "-";
            this.label_changeattranoil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(456, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 20);
            this.label16.TabIndex = 43;
            this.label16.Tag = "L4";
            this.label16.Text = "-";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_changeatDiff
            // 
            this.label_changeatDiff.AutoSize = true;
            this.label_changeatDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_changeatDiff.Location = new System.Drawing.Point(456, 166);
            this.label_changeatDiff.Name = "label_changeatDiff";
            this.label_changeatDiff.Size = new System.Drawing.Size(14, 20);
            this.label_changeatDiff.TabIndex = 42;
            this.label_changeatDiff.Tag = "L3";
            this.label_changeatDiff.Text = "-";
            this.label_changeatDiff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(456, 250);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 20);
            this.label18.TabIndex = 45;
            this.label18.Tag = "L6";
            this.label18.Text = "-";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(456, 222);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 20);
            this.label19.TabIndex = 44;
            this.label19.Tag = "L5";
            this.label19.Text = "-";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(456, 306);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 20);
            this.label20.TabIndex = 47;
            this.label20.Tag = "L8";
            this.label20.Text = "-";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(456, 278);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 20);
            this.label21.TabIndex = 46;
            this.label21.Tag = "L7";
            this.label21.Text = "-";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(456, 362);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 20);
            this.label22.TabIndex = 49;
            this.label22.Tag = "L10";
            this.label22.Text = "-";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(456, 334);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 20);
            this.label23.TabIndex = 48;
            this.label23.Tag = "L9";
            this.label23.Text = "-";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(456, 390);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 20);
            this.label25.TabIndex = 50;
            this.label25.Tag = "L11";
            this.label25.Text = "-";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textbox_notes
            // 
            this.textbox_notes.Location = new System.Drawing.Point(161, 444);
            this.textbox_notes.Name = "textbox_notes";
            this.textbox_notes.Size = new System.Drawing.Size(339, 96);
            this.textbox_notes.TabIndex = 23;
            this.textbox_notes.Tag = "notes";
            this.textbox_notes.Text = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(80, 447);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 54;
            this.label17.Text = "Notes:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Window;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(558, 24);
            this.menuStrip1.TabIndex = 55;
            this.menuStrip1.Text = "menu_top";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.loadToolStripMenuItem.Text = "Open";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeCarNameToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // changeCarNameToolStripMenuItem
            // 
            this.changeCarNameToolStripMenuItem.Enabled = false;
            this.changeCarNameToolStripMenuItem.Name = "changeCarNameToolStripMenuItem";
            this.changeCarNameToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.changeCarNameToolStripMenuItem.Text = "Change Car Name";
            this.changeCarNameToolStripMenuItem.Click += new System.EventHandler(this.changeCarNameToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 541);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textbox_notes);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label_changeatDiff);
            this.Controls.Add(this.label_changeattranoil);
            this.Controls.Add(this.label_changeatoil);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textbox_changebrakefluid);
            this.Controls.Add(this.textbox_changesparkplugs);
            this.Controls.Add(this.textbox_changeaircleaner);
            this.Controls.Add(this.textbox_changefuel);
            this.Controls.Add(this.textbox_changecoolant);
            this.Controls.Add(this.textbox_changetiming);
            this.Controls.Add(this.textbox_changedrivebelts);
            this.Controls.Add(this.textbox_changetirerot);
            this.Controls.Add(this.textbox_changediffoil);
            this.Controls.Add(this.textbox_changetranoil);
            this.Controls.Add(this.textbox_changeengineoil);
            this.Controls.Add(this.label_title);
            this.Controls.Add(this.textbox_brakefluid);
            this.Controls.Add(this.textbox_sparkplugs);
            this.Controls.Add(this.textbox_aircleaner);
            this.Controls.Add(this.textbox_fuelfilter);
            this.Controls.Add(this.textbox_coolant);
            this.Controls.Add(this.textbox_timingbelt);
            this.Controls.Add(this.textbox_drivebelts);
            this.Controls.Add(this.textbox_tirerot);
            this.Controls.Add(this.textbox_diffoil);
            this.Controls.Add(this.textbox_tranoil);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textbox_engineoil);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Car Maintenance Tracker";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textbox_engineoil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textbox_tranoil;
        private System.Windows.Forms.TextBox textbox_diffoil;
        private System.Windows.Forms.TextBox textbox_tirerot;
        private System.Windows.Forms.TextBox textbox_drivebelts;
        private System.Windows.Forms.TextBox textbox_timingbelt;
        private System.Windows.Forms.TextBox textbox_coolant;
        private System.Windows.Forms.TextBox textbox_fuelfilter;
        private System.Windows.Forms.TextBox textbox_aircleaner;
        private System.Windows.Forms.TextBox textbox_sparkplugs;
        private System.Windows.Forms.TextBox textbox_brakefluid;
        private System.Windows.Forms.Label label_title;
        private System.Windows.Forms.TextBox textbox_changebrakefluid;
        private System.Windows.Forms.TextBox textbox_changesparkplugs;
        private System.Windows.Forms.TextBox textbox_changeaircleaner;
        private System.Windows.Forms.TextBox textbox_changefuel;
        private System.Windows.Forms.TextBox textbox_changecoolant;
        private System.Windows.Forms.TextBox textbox_changetiming;
        private System.Windows.Forms.TextBox textbox_changedrivebelts;
        private System.Windows.Forms.TextBox textbox_changetirerot;
        private System.Windows.Forms.TextBox textbox_changediffoil;
        private System.Windows.Forms.TextBox textbox_changetranoil;
        private System.Windows.Forms.TextBox textbox_changeengineoil;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label_changeatoil;
        private System.Windows.Forms.Label label_changeattranoil;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label_changeatDiff;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RichTextBox textbox_notes;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeCarNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

