﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; 

/*
 * lastChanged array is layed out like so:
 * [0]-> engine oil, [1]-> tran. oil, [2]-> diff. oil, etc...
 * The textboxes list is sorted to match this ordering.
 */
namespace Car_Maintenance_Tracker
{
    public partial class Form1 : Form
    {
        // === Private variables ===
        private String filename = null;          // The file that is being used
        private List<TextBox> textboxes = new List<TextBox>(); // List of all textboxes
        private List<Label> changeAt = new List<Label>();  // List of "Change At" labels

        public Form1()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            InitializeComponent();
            foreach (Control ctrl in this.Controls) // Add textboxes and labels to their lists
            {
                if (ctrl is TextBox) // Get the textboxes
                {
                    if (Convert.ToInt32(ctrl.Tag) < 23)
                        textboxes.Add((TextBox)ctrl);
                }
                else if (ctrl is Label) // Get the labels
                {
                    if (Convert.ToString(ctrl.Tag).StartsWith("L"))
                        changeAt.Add((Label)ctrl);
                }

            }
            // Sort items to match ordering of lastChange[] array.
            textboxes = textboxes.OrderBy(x => Convert.ToInt32(x.Tag)).ToList();
            changeAt = changeAt.OrderBy(x => x.TabIndex).ToList();
        }

        // ===========================
        //          SAVING
        // ===========================
        /// <summary>
        /// Save the current data into the original file. (File -> Save)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // A new car was created (therefore no file is open). Save a new file.
            if (filename == null)
            {
                SaveFileDialog saveDialog = new SaveFileDialog(); // Open save dialog
                saveDialog.Filter = "Text Files (.txt)|*.txt";    // Limit to txt files
                saveDialog.RestoreDirectory = true;
                saveDialog.ShowDialog();
                filename = saveDialog.FileName; // Get the filename returned from the dialog box
                if (filename == null || filename.CompareTo("") == 0)
                {
                    MessageBox.Show("A file was not chosen.");
                    filename = null;
                    return;
                }
            }

            // A file was already loaded. Save over the old file.
            writeData(filename);
        }

        /// <summary>
        /// Save to a new, chosen file. (File -> Save As)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog(); // Open save dialog
            saveDialog.Filter = "Text Files (.txt)|*.txt";    // Limit to txt files
            saveDialog.RestoreDirectory = true;
            saveDialog.ShowDialog();
            filename = saveDialog.FileName; // Get the filename returned from the dialog box
            if (filename == null || filename.CompareTo("") == 0)
            {
                MessageBox.Show("A file was not chosen.");
                filename = null;
                return;
            }
            writeData(filename);
        }

        // ===========================
        //          SAVING
        // ===========================

        /// <summary>
        /// Write data to a file
        /// </summary>
        /// <param name="filename">The filename to write to</param>
        private void writeData(string filename)
        {
            if (filename == null)
            {
                MessageBox.Show("No file selected");
                return;
            }
            try
            {
                System.IO.StreamWriter newFile = new System.IO.StreamWriter(filename);
                newFile.WriteLine(label_title.Text); // Write car title
                foreach (TextBox textbox in textboxes) // Write textbox values to file
                {
                    newFile.WriteLine(textbox.Text);
                }
                newFile.Write(textbox_notes.Text); // Write the notes in the bottom box
                newFile.Close();
                String[] temp = filename.Split('\\');
                MessageBox.Show(temp[temp.Length - 1] + " saved"); // Tell user that file was saved
                return;
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Load a car's file into memory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text Files (.txt)|*.txt";
            dialog.ShowDialog();
            filename = dialog.FileName;

            if (filename.CompareTo("") == 1) // If user actually chose a file
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(filename);

                String line = null;
                int lineNumber = 1;
                double dummy; // A dummy variable for try parse method
                try
                {
                    int i = 0;
                    label_title.Text = reader.ReadLine(); // Get vehicle title
                    while (i < textboxes.Count) // Read numbers for textboxes
                    {
                        lineNumber++;
                        line = reader.ReadLine();

                        if (!Double.TryParse(line, out dummy)) throw new FormatException();
                        textboxes[i].Text = line;
                        i++;
                    }
                    textbox_notes.Text = reader.ReadToEnd(); // Read text for 'notes' box
                }
                catch (System.IO.IOException ex)
                {
                    MessageBox.Show(ex.Message); // File wasn't found. Or something else. Hell if I know...
                }
                catch (System.FormatException) // A piece of crap value was found. Throw error and don't continue loading.
                {
                    MessageBox.Show("Invalid value found on line " + lineNumber + ": \"" + line + "\"");
                    filename = null;
                    label_title.Text = "Load a file to start...";
                    foreach (TextBox t in textboxes) t.Text = "";
                    return;
                }
                reader.Close(); // We're done with the file now

                // Update the "Change At" labels
                int j = 0;
                foreach (Label l in changeAt)
                {
                    l.Text = Convert.ToString(Convert.ToUInt32(textboxes[j].Text) + Convert.ToUInt32(textboxes[j + 11].Text));
                    j++;
                }
                changeCarNameToolStripMenuItem.Enabled = true; // Enable the 'Change' button, aaaaaaaaaaaaaaand we're done!
            }
        }

        /// <summary>
        /// Create the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // Get a generic TextChanged event handler for every textbox
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox)
                {
                    TextBox tb = (TextBox)ctrl;
                    tb.TextChanged += new EventHandler(tb_TextChanged);
                }
            }
        }

        /// <summary>
        /// Generic event that is fired whenever any textbox is modified
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb_TextChanged(object sender, EventArgs e)
        {
            TextBox textbox = (TextBox)sender;
            int i = textbox.TabIndex - 1;      // Figure out which textbox was changed
            if (i >= 11) i -= 11;              // An 'interval' textbox was changed. Adjust value.
            try
            {
                int num1 = Convert.ToInt32(textboxes[i].Text);
                if (num1 == 0) changeAt[i].Text = "-";
                else
                {
                    textboxes[i].BackColor = Color.White; // Success. Make sure box is white
                    int num2 = Convert.ToInt32(textboxes[i + 11].Text);
                    changeAt[i].Text = Convert.ToString(num1 + num2);
                }
                textboxes[i + 11].BackColor = Color.White;
            }
            catch (System.FormatException) // Something weird was entered
            {
                // Check if exception was caused by bad input or just an empty textbox
                int crap; // A crap variable
                if (textboxes[i].Text != "" &&
                    Int32.TryParse(textboxes[i].Text, out crap) == false)
                {
                    textboxes[i].BackColor = Color.Red; // Box on left is bad
                }
                if (textboxes[i + 11].Text != "" &&
                    Int32.TryParse(textboxes[i + 11].Text, out crap) == false)
                {
                    textboxes[i + 11].BackColor = Color.Red; // Box on right is bad
                }
            }
        }

        /// <summary>
        /// Called when the 'new' button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InputForm a = new InputForm(this, true);
            a.Show();
        }

        // =============================
        //       PUBLIC FUNCTIONS
        // =============================
        /// <summary>
        /// Erase all fields to prepare for a new car
        /// </summary>
        /// <param name="carname">The name of the new car</param>
        public void newCar(String carname)
        {
            changeCarNameToolStripMenuItem.Enabled = true; // Make sure change button is on
            label_title.Text = carname;
            textbox_notes.Text = "";
            filename = null;
            foreach (TextBox t in textboxes) // Clear the textboxes
            {
                t.Text = "";
            }
            foreach (Label l in changeAt) // Clear the "Change At" labels
            {
                l.Text = "-";
            }
        }

        /// <summary>
        /// Change the car name displayed at the top of the windows
        /// </summary>
        /// <param name="carname"></param>
        public void changeCarname(String carname)
        {
            label_title.Text = carname;
        }

        /// <summary>
        /// Event called when user chooses File -> New
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeCarNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InputForm a = new InputForm(this, false);
            a.Show();
        }

        /// <summary>
        /// Exit the program when the user chooses File -> Exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Open the 'About' window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow(this, this.Location);
            aboutWindow.ShowDialog();
        }
    }
}
