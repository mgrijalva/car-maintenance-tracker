﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Car_Maintenance_Tracker
{
    public partial class AboutWindow : Form
    {
        Form parentForm;

        public AboutWindow(Form parent, Point startPosition)
        {
            InitializeComponent();
            startPosition.X += 60;
            startPosition.Y += 60;
            this.Location = startPosition;
            parentForm = parent;
        }

        private void label_githubLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/mgrijalva/Car_Maintenance_Tracker");
        }
    }
}
