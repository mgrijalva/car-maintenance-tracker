﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Car_Maintenance_Tracker
{
    public partial class InputForm : Form
    {
        private Form1 form1; // Handle to the main form
        private bool newCar; // Make a new car (true)? Or just changing the title (false)?

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="f">The parent form</param>
        /// <param name="newcar">Am I creating a new car?</param>
        public InputForm(Form1 f, bool newcar)
        {
            newCar = newcar;
            InitializeComponent();
            form1 = f;
            this.Location = new Point(form1.Location.X + 100, form1.Location.Y + 100);
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (newCar) form1.newCar(textbox_input.Text);
            else form1.changeCarname(textbox_input.Text);
            this.Close();
            this.Dispose();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
